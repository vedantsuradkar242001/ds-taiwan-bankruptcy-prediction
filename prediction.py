import gzip
import json
import pandas as pd
import seaborn as sns
import numpy as np
from sklearn.impute import SimpleImputer
import matplotlib.pyplot as plt
from sklearn.metrics import ConfusionMatrixDisplay
from imblearn.over_sampling import RandomOverSampler
from imblearn.under_sampling import RandomUnderSampler
from sklearn.model_selection import train_test_split
from sklearn.pipeline import make_pipeline
from sklearn.tree import DecisionTreeClassifier
import pickle
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import GridSearchCV, cross_val_score, train_test_split
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.metrics import (
    ConfusionMatrixDisplay,
    classification_report,
    confusion_matrix,
)

def wrangle(filename):

	with open(filename,"r") as read_file:
		data = json.load(read_file)

	df = pd.DataFrame().from_dict(data["data"]).set_index("company_id")

	return df

df = wrangle("./data.json.gz")

df["Bankrupt?"].value_counts(normalize=True).plot(
    kind="bar"
);

corr = df.drop(columns="Bankrupt?").corr()
corr.head()

target = df["Bankrupt?"]
X = df.drop(columns="Bankrupt?")
y = target
print("X shape:", X.shape)
print("y shape:", y.shape)

X_train, X_test, y_train, y_test = train_test_split(
    X, y, test_size=0.2, random_state=42
)

print("X_train shape:", X_train.shape)
print("y_train shape:", y_train.shape)
print("X_test shape:", X_test.shape)
print("y_test shape:", y_test.shape)

under_sampler = RandomUnderSampler(random_state=42)
X_train_under, y_train_under = under_sampler.fit_resample(X_train, y_train)
print(X_train_under.shape)
X_train_under.head()

over_sampler = RandomOverSampler(random_state=42)
X_train_over, y_train_over = over_sampler.fit_resample(X_train, y_train)
print(X_train_over.shape)
X_train_over.head()

acc_baseline = y_train.value_counts(normalize=True).max()
print("Baseline Accuracy:", round(acc_baseline, 4))

model_reg = make_pipeline(
    SimpleImputer(strategy="median"), DecisionTreeClassifier(random_state=42)
)
model_reg.fit(X_train, y_train)

#Fit on 'X_train_under', 'y_train_under'
model_under = make_pipeline(
    SimpleImputer(strategy="median"), DecisionTreeClassifier(random_state=42)
)
model_under.fit(X_train_under, y_train_under)

#Fit on 'X_train_over', 'y_train_over'
model_over = make_pipeline(
    SimpleImputer(strategy="median"), DecisionTreeClassifier(random_state=42)
)
model_over.fit(X_train_over, y_train_over)

for m in [model_reg, model_under, model_over]:
    acc_train = m.score(X_train, y_train)
    acc_test = m.score(X_test, y_test)
    
    print("Training Accuracy:", round(acc_train, 4))
    print("Test Accuracy:", round(acc_test, 4))

#random forest
clf = make_pipeline(SimpleImputer(),RandomForestClassifier(random_state=42))
params ={
	"simpleimputer_startegy":["mean","median"],
	"randomforestclassifier__n_estimators":range(25,100,25),
	"randomforestclassifier__max_depth":range(10,50,10)
}
RFmodel = GridSearchCV(
	clf,
	param_grid=params,
	cv=5,
	n_jobs=-1,
	verbose=1)

RFmodel.fit(X_train_over, y_train_over)
cv_results = pd.DataFrame(RFmodel.cvresults_)
cv_results.head()


# Gradient boosting
clf2 = make_pipeline(SimpleImputer(),GradientBoostingClassifier())
params2 ={
	"simpleimputer_startegy":["mean","median"],
	"gradientboostinfgclassifier__n_estimators":range(25,31,5),
	"gradientboostinfgclassifier__max_depth":range(2,5)
}
GBmodel = GridSearchCV(
	clf,
	param_grid=params,
	cv=5,
	n_jobs=-1,
	verbose=1)

GBmodel.fit(X_train_over, y_train_over)
cv_results2 = pd.DataFrame(GBmodel.cvresults_)
cv_results2.head()

ConfusionMatrixDisplay.from_estimator(model_reg, X_test, y_test);
depth = model_over.named_steps["decisiontreeclassifier"].get_depth()
print(depth)


print(classification_report(y_test, model.predict(X_test)))
importances = model_over.named_steps["decisiontreeclassifier"].feature_importances_

#put importances into a series
feat_imp = pd.Series(importances, index=X_train_over.columns).sort_values()

#plot series
feat_imp.tail(15).plot(kind="barh")
plt.xlabel("Gini Importance")
plt.ylabel("Feature")
plt.title("model_over Feature Importance");

